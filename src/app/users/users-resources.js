'use strict';

angular
  .module('project')
  .factory('Users', function ($resource) {
    return $resource(
      'http://192.168.1.142:3000/twitters/:id',
      {id: '@id'}
    )
  });