(function() {
  'use strict';

  angular
    .module('project')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/home',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
      }).state('register', {
        url: '/register',
        templateUrl: 'app/register/register.html',
        controller: 'RegisterController',
      }).state('login', {
        url: '/login',
        templateUrl: 'app/login/login.html',
        controller: 'LoginController',
      }).state('users', {
        url: '/users',
        templateUrl: 'app/users/users.html'
        //controller: 'LoginController',
      });

    $urlRouterProvider.otherwise('/home');
  }

})();
