'use strict';

angular
  .module('project')
  .factory('Session', function ($http, $window, $state) {

    return {
      access: function (twitter, callback) {
        
        $http({
          method: 'post',
          url:'http://192.168.1.142:3000/twitters',
          data: twitter
        })
        .success(function (data) {        
          if (callback) callback(data);
          console.log(data)
          $state.go('login');
          //$window.sessionStorage.setItem('twitter', data.id);        
        }); 
      },
    }
  });