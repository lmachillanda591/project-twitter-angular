'use strict';

angular
  .module('project')
  .factory('Login', function ($http, $window, $state) {

    return {
      access: function (user, callback) {
        
        $http({
          method: 'post',
          url:'http://192.168.1.142:3000/access',
          data: user
        })
        .success(function (data) {        
          if (callback) callback(data);
          console.log(data)
          $state.go('users');
          $window.sessionStorage.setItem('user', data.token);        
        }); 
      },
      isConnected: function () {
        return !!$window.sessionStorage.user;
      }
    }
  });