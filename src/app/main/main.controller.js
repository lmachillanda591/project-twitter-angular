(function() {
  'use strict';

  angular
    .module('project')
    .config(function ($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.interceptors.push(
      function ($q, $window) {
        console.log('aca');
        return {
      request: function(config) {
          config.headers = config.headers || {};
          config.headers['Content-Type'] = 'application/json';
          config.headers['Accept'] = 'application/json';
          if ($window.sessionStorage.user) {
            console.log('aca');
            config.headers['X-Token'] = $window.sessionStorage.user;
            }
            return config;
          }
        };
      })
  })
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, webDevTec, toastr, $state, $scope, Users) {

    $scope.awesomeThings = [];
    $scope.classAnimation = '';
    $scope.creationDate = 1456451119842;
    $scope.showToastr = showToastr;
    $scope.goToRegister = goToRegister;
    $scope.goToLogin = goToLogin;
    $scope.twitter = {
      user_attributes:{}
    };
    $scope.user = {};
    $scope.users = [];
    activate();

    function activate() {
      getWebDevTec();
      $timeout(function() {
        $scope.classAnimation = 'rubberBand';
      }, 4000);
    }

    function showToastr() {
      //toastr.info('Fork <a href="https://github.com/Swiip/generator-gulp-angular" target="_blank"><b>generator-gulp-angular</b></a>');
      $scope.classAnimation = '';
    }

    function getWebDevTec() {
      $scope.awesomeThings = webDevTec.getTec();

      angular.forEach($scope.awesomeThings, function(awesomeThing) {
        awesomeThing.rank = Math.random();
      });
    }

    function goToRegister() {
      $state.go('register'); 
    }

    function goToLogin() {
      $state.go('login'); 
    }

    function goTohome() {
      $state.go('home'); 
    };
  }
})();
